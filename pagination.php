<?php
        $showRecordPerPage = 15; //Кол-во строк

        if(isset($_GET['page']) && !empty($_GET['page'])){
            $currentPage = $_GET['page'];
        }else{
            $currentPage = 1;
        }
        $startFrom = ($currentPage * $showRecordPerPage) - $showRecordPerPage;
        $totalEmpSQL = "SELECT * FROM persons";
        $allEmpResult = mysqli_query($conn, $totalEmpSQL);
        $totalEmployee = mysqli_num_rows($allEmpResult);
        $lastPage = ceil($totalEmployee/$showRecordPerPage);
        $firstPage = 1;
        $nextPage = $currentPage + 1;
        $previousPage = $currentPage - 1;
?>
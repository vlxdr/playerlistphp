<!--Главный файл на вывод таблицы игроков -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p&family=M+PLUS+Rounded+1c:wght@100;300;400;500&display=swap" rel="stylesheet">
    <script src="js/script.js"></script>
    <title>Player list</title>
</head>
<body>
    <?php 
        require 'connect.php'; //Подключение к БД

        require 'pagination.php'; //Пагинация

        $sql_player_list = "SELECT * FROM `persons` ORDER BY `name` ASC LIMIT $startFrom, $showRecordPerPage"; //SQL запрос к таблице persons

        //Создаем список при помощи цикла
        if($result = $conn->query($sql_player_list)) {
            echo "<div class=\"breadcrumb_main\">" . "<a style=\"font-weight: 600; margin-right: 5px; border-bottom: 1px solid #383838;\">" . "Список игроков ". "</a>" . " / " . "<a id=\"breadcrump_hov\" href=\"tournaments.php\" style=\"margin-left: 5px; font-weight: 600; color: #a9a9a9; \">" . "Турниры" . "</a>" . "</div>";

            echo "<div class=\"searchContainer\">" . "<i class=\"bi bi-search\" id=\"iconSearch\">" . "</i>" . "<input type=\"text\" id=\"myInput\" onkeyup=\"table_search()\" placeholder=\"Поиск по ФИО...\" title=\"Введите имя\"\>" . "</div>";
                echo "<table class=\"table table-borderless table-hover\" id=\"table\">
                    <thead class=\"table-dark\" id=\"main_thead_stycky\">
                        <tr>
                            <th>№</th>
                            <th>Имя</th>
                            <th>Игрок</th>
                            <th>Тренер</th>
                            <th>Судья</th>
                        </tr>
                    </thead>";
                $p = 1;

            foreach($result as $row_player) {
                $id_player_convert = bin2hex($row_player["_ID"]); //Передаем конвертируемый _ID в ссылку при переходе по ссылке (выбранному игроку) 
                echo "<tr>";
                        echo "<td>" . $p . "</td>";
                        echo "<td>" . "<a href=\"statistic.php?id=$id_player_convert\">" . $row_player["name"] . "</a>" . "</td>"; //Передаем ID и переходим по ссылке к статистике игрока, используя расшифрованный ID
                            if($row_player["is_player"] == 0) {
                                echo "<td>" . "<i class=\"bi bi-x-lg\">" . "</i>" . "</td>";
                            }else {
                                echo "<td>" . "<i class=\"bi bi-check-lg\">" . "</i>" . "</td>";
                            }
                            if($row_player["is_trainer"] == 0) {
                                echo "<td>" . "<i class=\"bi bi-x-lg\">" . "</i>" . "</td>";
                            }else {
                                echo "<td>" . "<i class=\"bi bi-check-lg\">" . "</i>" . "</td>";
                            }
                            if($row_player["is_judge"] == 0) {
                                echo "<td>" . "<i class=\"bi bi-x-lg\">" . "</i>" . "</td>";
                            }else {
                                echo "<td>" . "<i class=\"bi bi-check-lg\">" . "</i>" . "</td>";
                            }
                    echo "</tr>";
                    $p++;
            }
            echo "</table>";
        } 

        mysqli_close($conn);   

            require 'num_pagination.html';
    ?>

<script>
    function table_search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("a")[0];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }       
    }
  }
</script>
</body>
</html>
<!DOCTYPE html>
<html lang="ru">
<head>  
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p&family=M+PLUS+Rounded+1c:wght@100;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/script.js"></script>
    <title>Statistic</title>
</head>
<body>

    <?php
        require 'connect.php';

        //ВЫВВОДИМ СТАТИСТИКУ ПО ИГРОКУ
        $player_id = $_GET['id']; //Получаем конвертированый ID игрока

        //Выполняем запрос по ID игрока
        $sql_statistic = "SELECT * FROM persons WHERE _ID = X'$player_id'";

        echo "<div class=\"Label_One\">" . "Информация" . "</div>";
        // echo "<a href=\"index.php\">" . "<div class=\"arrowBack\">" . "<i class=\"bi bi-arrow-left\">" . "</i>" . "</div>" . "</a>";
        
        //Статистика
        if($result = $conn->query($sql_statistic)) {
            foreach($result as $row_stat) {

                //Ковертация даты (возможно неправильно)
                $birthday = $row_stat["birthday"];
                $date_birthday = new DateTime($birthday);

                echo "<div class=\"breadcrumb\">" . "<a href=\"index.php\" class=\"_breadcrumb\">" . "Список игроков ". "</a>" . " / " . "<a style=\"font-weight: 600; margin-left: 5px; margin-right: 5px; border-bottom: 1px solid #383838;\">" . $row_stat["name"] . "</a>" . "/" . "<a href=\"tournaments.php\" class=\"_breadcrumb\">" . "Турниры" . "</a>" . "</div>";

                echo "<div class=\"animate__animated animate__fadeIn\" id=\"main\">";
                echo "<div id=\"image\">" . "</div>";
                    echo "<div class=\"info\">";
                        echo "<H4>" . $row_stat["name"] . "</H4>";

                        if($row_stat["is_player"] == 0) {
                                echo "" . "" . "";
                            }else {
                                echo "<p class=\"role_player\">" . "Игрок" . "</p>";
                            }
                        if($row_stat["is_trainer"] == 0) {
                                echo "" . "" . "";
                            }else {
                                echo "<p class=\"role_trainer\">" . "Тренер" . "</p>";
                            }
                        if($row_stat["is_judge"] == 0) {
                                echo "" . "" . "";
                            }else {
                                echo "<p class=\"role_judge\">" . "Судья" . "</p>";
                            }
                    echo "<pre class=\"info_label\">" . "Дата рождения: " . "<p>" . date("d.m.Y", strtotime($birthday)) . "</p>" . "</pre>";
                    echo "<pre class=\"info_label\">" . "Город: " . "<p>" . $row_stat["city"] . "</p>" . "</pre>";
                echo "</div>";

            }
        }

        
        //ВЫВОДИМ ТАБЛИЦУ ИГР (ТУРНИРОВ)

        //Хлебыне крошки
        // echo "<div style=\"font-weight: 600; margin-bottom: 5px;\">" . "ИНФО" . "</div>";
        

        //Выполняем запрос для вывода таблицы
        $sql_table = "SELECT t.name as tournament_name,
        (SELECT COUNT(*) AS passes FROM stats_goals WHERE assistant = X'$player_id') as passes,
        (SELECT COUNT(_PARENT) AS col_game FROM protocol_players WHERE player = X'$player_id') as col_game,
        (SELECT COUNT(delete_form) AS col_del FROM stats_deletes WHERE player = X'$player_id') as col_del,
        
            COUNT(*) as goals,
        
                p.name as prot, 
                te.name as team_name, 
                pd.play_date as play_date, 
                pc.play_city as play_city, 
                pa.num as num_p, 
                pls.players as players,
                lg.play_league as league 
        
            FROM stats_goals as g
        
                LEFT JOIN tournaments as t
                    ON g.tournament = t._ID
                LEFT JOIN protocols as p
                    ON p._ID = g.protocol
                LEFT JOIN teams as te
                    ON te._ID = g.team
                LEFT JOIN protocols as pd
                    ON g.protocol = pd._ID
                LEFT JOIN protocols as pc
                    ON g.protocol = pc._ID
                LEFT JOIN protocols as pa 
                    ON g.protocol = pa._ID
                LEFT JOIN protocols as pls
                    ON g.protocol = pls._ID
                LEFT JOIN tournaments as lg
                    ON g.tournament = lg._ID
        
            WHERE g.player = X'$player_id'";
        //

        //Формируем первую таблицу
        if($result = $conn->query($sql_table)){
            echo "<div class=\"tables\">";
            echo "<table class=\"table table-striped table-borderless table-hover\" id=\"table\">
            <tbody>
              <thead class=\"table-dark\">
                <tr>
                  <th>Турнир</th>
                  <th>Лига</th>
                  <th>Игры</th>
                  <th>Пасы</th>
                  <th>Голы</th>
                  <th>Штрафы</th>
                </tr>
              </thead>
            </tbody>";
            $i = 1;
            foreach($result as $row_table){
                // $id_tour_convert = bin2hex($row_table["_ID"]);
                echo "<tr>";
                    echo "<td onclick=\"hide_table()\">" . "<a href=\"#tour_name\">" . $row_table["tournament_name"] . "</a>" . "</td>";
                    echo "<td>" . $row_table["league"] . "</td>";
                    echo "<td>" . $row_table["col_game"] . "</td>";
                    echo "<td>" . $row_table["passes"];
                    echo "<td>" . $row_table["goals"] . "</td>";
                    echo "<td>" . $row_table["col_del"] . "</td>";
                echo "</tr>";
                $i++;
            }
                    echo "</table>";
                echo "</div>";
            echo "</div>";
        }

        echo "<div class=\"\" id=\"animate_block\">";
        echo "<div class=\"Label_Two\" id=\"hide_label\">" . "Статистика по турниру" . "</div>";

        //Формируем вторую таблицу
        if($result = $conn->query($sql_table)){
            echo "<div class=\"tables_second\" id=\"hide_table\">";
            echo "<table class=\"table table-striped table-borderless table-hover\" id=\"table\">
            <tbody>
              <thead class=\"table-dark\">
                <tr>
                  <th>Название турнира</th>
                  <th>Дата игры</th>
                  <th>Протокол</th>
                  <th>Город</th>
                  <th>Номер протокола</th>
                  <th>Кол-во игроков</th>
                  <th>Команда</th>
                </tr>
              </thead>
            </tbody>";
            $i = 1;
            foreach($result as $row_table_second){

                //Ковертация даты (возможно неправильно)
                $play_date = $row_table_second["play_date"];
                $date_play = new DateTime($play_date);

                echo "<tr>";
                    echo "<td id=\"tour_name\">" . $row_table_second["tournament_name"] . "</td>";
                    echo "<td>" . date("d.m.Y", strtotime($play_date)) . "</td>";
                    echo "<td>" . $row_table_second["prot"] . "</td>";
                    echo "<td>" . $row_table_second["play_city"] . "</td>";
                    echo "<td>" . $row_table_second["num_p"] . "</td>";
                    echo "<td>" . $row_table_second["players"] . "</td>";
                    echo "<td>" . $row_table_second["team_name"] . "</td>";
                echo "</tr>";
                $i++;
            }
            echo "</div>";
            echo "</div>";
          }
        
        mysqli_close($conn); //Закрываем соединение
    ?>

</body>
</html>
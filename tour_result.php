<!--Главный файл на вывод таблицы турниров -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p&family=M+PLUS+Rounded+1c:wght@100;300;400;500&display=swap" rel="stylesheet">
    <script src="js/script.js"></script>
    <title>Tournaments result</title>
</head>
<body>
    <?php 
        require 'connect.php'; //Подключение к БД
        $id_tournament_convert = $_GET['id'];

        $sql_tournaments_result = "SELECT * FROM `stats_results` WHERE `_ID` = X'$id_tournament_convert'";

        $sql_tournaments = "SELECT t.name as tournament_name, 
        
                p.name as prot, 
                gt.game_time as game_t, 
                sa.score_a as sc_a, 
                sb.score_b as sc_b, 
                pls.players as pl,
                tma.name as tmma,
                tmb.name as tmmb
        
            FROM stats_results as sg
            
                LEFT JOIN tournaments as t 
                    ON sg.tournament = t._ID
                LEFT JOIN protocols as p
                    ON p._ID = sg.protocol
                LEFT JOIN stats_results as gt
                    ON gt.game_time = sg.game_time
                LEFT JOIN stats_results as sa
                    ON sa.score_a = sg.score_a
                LEFT JOIN stats_results as sb
                    ON sb.score_b = sg.score_b
                LEFT JOIN stats_results as pls
                    ON pls.players = sg.players
                LEFT JOIN teams as tma
                    ON sg.team_a = tma._ID
                LEFT JOIN teams as tmb
                    ON sg.team_b = tmb._ID

            WHERE sg.tournament = X'$id_tournament_convert'";

        //Создаем список при помощи цикла
        if($result = $conn->query($sql_tournaments)) {
            echo "<div class=\"breadcrumb_main\">" . "<a href=\"index.php\" class=\"_breadcrumb\">" . 
            "Список игроков ". "</a>" . " / " . "<a href=\"tournaments.php\" class=\"_breadcrumb\">" . 
            "Турниры" . "</a>" . "/" . "<a style=\"font-weight: 600; margin-left: 5px; border-bottom: 1px solid #383838;\">" . 
            "Информация о турнире" . "</a>" . "</div>";

                echo "<table class=\"table table-borderless table-hover\" style=\"margin-top: 65px\">
                    <thead class=\"table-dark\">
                        <tr>
                            <th>№</th>
                            <th>Протокол</th>
                            <th>Событие в секундах</th>
                            <th>Счет команды А</th>
                            <th>Счет команды Б</th>
                            <th>Кол-во игроков</th>
                            <th>Команда А</th>
                            <th>Команда Б</th>
                        </tr>
                    </thead>";
                $t = 1;

            foreach($result as $row_tour_r) {
                
                echo "<tr>";
                        echo "<td>" . $t . "</td>";
                            // echo "<td class=\"sticky_left_table\">" . $row_tour_r["tournament_name"] . "</td>";
                            echo "<td>" . $row_tour_r["prot"] . "</td>";
                            echo "<td>" . $row_tour_r["game_t"] . "</td>";
                            echo "<td>" . $row_tour_r["sc_a"] . "</td>";
                            echo "<td>" . $row_tour_r["sc_b"] . "</td>";
                            echo "<td>" . $row_tour_r["pl"] . "</td>";
                            echo "<td>" . $row_tour_r["tmma"] . "</td>";
                            echo "<td>" . $row_tour_r["tmmb"] . "</td>";
                    echo "</tr>";
                    $t++;
            }
            echo "</table>";
            echo "<h8 class=\"tournament_name\">" . $row_tour_r["tournament_name"] . "</h8>";
        } 

        mysqli_close($conn);   
    ?>
</body>
</html>
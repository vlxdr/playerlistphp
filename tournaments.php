<!--Главный файл на вывод таблицы турниров -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p&family=M+PLUS+Rounded+1c:wght@100;300;400;500&display=swap" rel="stylesheet">
    <script src="js/script.js"></script>
    <title>Tournaments list</title>
</head>
<body>
    <?php 
        require 'connect.php'; //Подключение к БД

        $sql_tournaments_list = "SELECT * FROM `tournaments` ORDER BY `name` ASC"; //SQL запрос к таблице tournaments




        //Создаем список при помощи цикла
        if($result = $conn->query($sql_tournaments_list)) {
            echo "<div class=\"breadcrumb_main\">" . "<a href=\"index.php\" class=\"_breadcrumb\">" . "Список игроков ". "</a>" . " / " . "<a style=\"font-weight: 600; margin-left: 5px; border-bottom: 1px solid #383838;\">" . "Турниры" . "</a>" . "</div>";

                echo "<table class=\"table table-borderless table-hover\" style=\"width: 325vh;\">
                    <thead class=\"table-dark\">
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Погодные условия</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                            <th>Кол-во таймов</th>
                            <th>Прод-ть 1-го тайма</th>
                            <th>Прод-ть 2-го тайма</th>
                            <th>Прод-ть 3-го тайма</th>
                            <th>Возможно назнач-е овертайма</th>
                            <th>Прод-ть 1-го овертайма</th>
                            <th>Прод-ть 2-го овертайма</th>
                            <th>Прод-ть таймаута</th>
                            <th>Компенси-ое время по таймауту</th>
                            <th>Кол-во возможных таймаутов</th>
                            <th>Прод-ть удаления белая карточка</th>
                            <th>Прод-ть удаления синяя карточка</th>
                            <th>Отсчет штрафного времени при 3-м удалении</th>
                            <th>При забитом мяче выход удаленного по умолчанию</th>
                            <th>Лига</th>
                            <th>Организатор</th>
                            <th>Есть заявка на турнир</th>
                        </tr>
                    </thead>";
                $t = 1;

            foreach($result as $row_tour) {
                $id_tournament_convert = bin2hex($row_tour["_ID"]);

                //Ковертация даты (возможно неправильно)
                $begin_date = $row_tour["begin_date"];
                $date_b = new DateTime($begin_date);

                $end_date = $row_tour["end_date"];
                $date_e = new DateTime($end_date);

                echo "<tr>";
                        echo "<td class=\"sticky_number_left_table\">" . $t . "</td>";
                            echo "<td class=\"sticky_left_table\">" . "<a href=\"tour_result.php?id=$id_tournament_convert\">" . $row_tour["name"] . "</a>" . "</td>";
                            echo "<td>" . $row_tour["weather"] . "</td>";
                            echo "<td>" . date("d.m.Y", strtotime($begin_date)) . "</td>";
                            echo "<td>" . date("d.m.Y", strtotime($end_date)) . "</td>";
                            echo "<td>" . $row_tour["period_cnt"] . "</td>";
                            echo "<td>" . $row_tour["period_1_dur"] . "</td>";
                            echo "<td>" . $row_tour["period_2_dur"] . "</td>";
                            echo "<td>" . $row_tour["period_3_dur"] . "</td>";
                            echo "<td>" . $row_tour["overtime"] . "</td>";
                            echo "<td>" . $row_tour["overtime_1_dur"] . "</td>";
                            echo "<td>" . $row_tour["overtime_2_dur"] . "</td>";
                            echo "<td>" . $row_tour["timeout_dur"] . "</td>";
                            echo "<td>" . $row_tour["compensate_timeout_dur"] . "</td>";
                            echo "<td>" . $row_tour["timeouts"] . "</td>";
                            echo "<td>" . $row_tour["deletes_w_dur"] . "</td>";
                            echo "<td>" . $row_tour["deletes_b_dur"] . "</td>";
                            echo "<td>" . $row_tour["countdown_on_third_delete"] . "</td>";
                            echo "<td>" . $row_tour["exit_deleted_on_goal"] . "</td>";
                            echo "<td>" . $row_tour["play_league"] . "</td>";
                            echo "<td>" . $row_tour["organizator"] . "</td>";
                            echo "<td>" . $row_tour["has_apply_for_tournament"] . "</td>";
                    echo "</tr>";
                    $t++;       
            }
            echo "</table>";
        }         
        mysqli_close($conn);   
    ?>
</body>
</html>